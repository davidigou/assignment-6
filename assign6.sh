#!/bin/bash

#DAVID IGOU V00505208


reD="\b[Dd].*[Dd]$"
reA="\b[Aa].*[A-Za-z]$"
reNum="^[0-9]*$"
reAlp="^[A-Za-z0-9]*$"

linecount=0
wordcount=0
count=0
Dcount=0
Acount=0
Numcount=0
Alpcount=0
counter=0
mincount=99999999999
maxcount=0
minword="ss"
maxword=""


filename=${1}
while read -r line
do
   linecount=$((linecount+1))

   words=${line}
   for word in $words
   do
	wordcount=$((wordcount+1)) 
	if [[ $word =~ $reA ]]; then	
#	    echo ' matching Aword: '$word
	    Acount=$((Acount+1))
	fi
	if [[ $word =~ $reD ]]; then
#	    echo ' matching Dword: '$word
	    Dcount=$((Dcount+1))
	fi
	if [[ $word =~ $reNum ]]; then
#	    echo ' matching Numword: '$word
	    Numcount=$((Numcount+1))
	fi
	if [[ $word =~ $reAlp ]]; then
#	    echo ' matching Alpword: '$word
	    Alpcount=$((Alpcount+1))
	fi

	counter=0
	while read line2
	do
		twords=${line2}
		for wurd in $twords
		do
		if [[ $word = $wurd ]]; then
			counter=$((counter+1))
		fi
		done
	done < ${filename}

#	echo "Occurances of $currentword $counter"
	
	if [[ $counter -gt $maxcount ]]; then
		maxcount=$counter
		maxword=$word
	fi
	if [[ $counter -lt $mincount ]]; then
		mincount=$counter
		minword=$word
	fi
	
	
	
	counter=0





    done 
done < ${filename}



echo "maxword $maxword at $maxcount"
echo "minword $minword at $mincount"
echo "linecount: $linecount"
echo "wordcount: $wordcount"

echo "words that start and end with d/D $Dcount"
echo "words that start with A $Acount"
echo "words that are numeric $Numcount"
echo "words that are alphanumeric $Alpcount"
